FROM alpine

RUN apk --update add --no-cache bash

RUN find /usr/local \
      \( -type d -a -name test -o -name tests \) \
      -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
      -exec rm -rf '{}' + \
    && rm -rf /var/cache/apk/*

RUN addgroup -S gitlab && adduser -S gitlab -G gitlab

USER gitlab
