# Docker Rootless Example

An example pipeline attempting to build and use a rootless Docker container.

Current challanges are:
- Gitlab CI clones everything as `root`
- Our scripts are not able to touch the cloned files limiting therefore actions on top of them ( for eg. `chmod +x` using a different user, in this case `gitlab` )
